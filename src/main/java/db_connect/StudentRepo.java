package db_connect;

import model.Student;

import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
        // Need to be implemented
        return false;
    }
    
    public List<Student> findStudents() {
        // Need to be implemented
        return null;
    }
    
    public boolean updateStudent(Student student) {
        // Need to be implemented
        return false;
    }
    
    public boolean deleteStudent(Student student) {
        // Need to be implemented
        return false;
    }
}
